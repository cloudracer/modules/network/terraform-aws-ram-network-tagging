resource "aws_ec2_tag" "shared_resource_tag" {
  resource_id = var.shared_resource_id
  for_each    = var.shared_resource_tags
  key         = each.key
  value       = each.value
}
