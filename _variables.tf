variable "shared_resource_tags" {
  type        = map(string)
  description = "Tags that the shared resource should inherit"
}

variable "shared_resource_id" {
  type        = string
  description = "Id of the shared resource to be tagged"
}
