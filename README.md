# Terraform AWS RAM/EC2 Network Tagging Module

## Module

### Copy tags of a shared resource from the sharing account to the shared account

```
module "ec2_network_tagging" {
  source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/network/terraform-aws-ram-network-tagging"

  providers = {
    aws = aws.sharedAccount
  }

  shared_resource_tags = resource.tags

  shared_resource_id = resource.id

}
```

### Usage 
For each resource that needs to be shared with a principal, provide a module call with the principal as a provider and the resource id and tags.

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.
